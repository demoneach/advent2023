package day1;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import helper.Helper;

/**
 * Day1
 */
public class Day1 {

    private static final Logger log = Logger.getLogger(Day1.class.getName());

    public int part1(List<String> lines) {
        int sum = 0;

        for (String line : lines) {
            line = line.replaceAll("\\D", "");

            sum += Integer.parseInt("%s%s".formatted(line.charAt(0), line.charAt(line.length() - 1)));
        }

        return sum;
    }

    public int part2(List<String> lines) {
        int sum = 0;
        HashMap<String, String> words = new HashMap<>();
        words.put("one", "1");
        words.put("two", "2");
        words.put("three", "3");
        words.put("four", "4");
        words.put("five", "5");
        words.put("six", "6");
        words.put("seven", "7");
        words.put("eight", "8");
        words.put("nine", "9");

        Pattern pattern = Pattern.compile("(?=(\\d|one|two|three|four|five|six|seven|eight|nine))");

        for (String line : lines) {
            Matcher matcher = pattern.matcher(line);
            List<String> matches = new ArrayList<>();

            while (matcher.find()) {
                matches.add(matcher.group(1));
            }

            String l1 = words.containsKey(matches.get(0)) ? words.get(matches.get(0)) : matches.get(0);
            String l2 = words.containsKey(matches.get(matches.size() - 1))
                    ? words.get(matches.get(matches.size() - 1))
                    : matches.get(matches.size() - 1);

            sum += Integer.parseInt("%s%s".formatted(l1, l2));
        }

        return sum;
    }

    public static void main(String[] args) {
        Day1 day1 = new Day1();
        List<String> lines = Helper.INSTANCE.readLinesFromFile("./day1/day1_input.txt");

        log.info("Day1 Part1: %d".formatted(day1.part1(lines)));
        log.info("Day1 Part2: %d".formatted(day1.part2(lines)));
    }
}